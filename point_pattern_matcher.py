from copy import deepcopy

from matplotlib import pyplot as plt

from helper.helpers import CartesianPointSet, PolarPointSet, CartesianPoints, CartesianPoint, is_equal


class PointPatternMatcher:
    def __init__(self, A: CartesianPointSet, B: CartesianPointSet):
        self._A = A
        self._B = B
        self._N = len(A.points)

    def get_A(self) -> CartesianPointSet:
        return self._A

    def get_B(self) -> CartesianPointSet:
        return self._B

    def get_N(self) -> int:
        return self._N

    def match(self) -> dict:
        def match_remainings(matching: dict) -> dict:
            if (len(matching.keys()) != self._N):
                for i in range(self._N):
                    if (not i in matching.keys()):
                        matching[i] = self._A[i].match(B.subtract([CartesianPoint(B[p].x, B[p].y) for p in matching.values()]))

            return matching

        B = deepcopy(self._B)

        try:
            self.scale_factor = self._A.calc_smallst_enclos_circle().radius / self._B.calc_smallst_enclos_circle().radius
            B.scale(self.scale_factor)
        except:
            pass

        self.translation_offset = self._A.calc_centroid() - B.calc_centroid()
        B.transform(self.translation_offset)

        matching = {}

        centroid = self._A.calc_centroid()

        for i in range(self._N):
            a_i_polar = self._A[i].convert_to_polar_point(centroid)

            for j in range(self._N):
                b_j_polar = B[j].convert_to_polar_point(centroid)

                if is_equal(a_i_polar.phi, b_j_polar.phi, err=10) and is_equal(a_i_polar.r, b_j_polar.r, err=1):
                    matching[i] = j

        matching = match_remainings(matching)

        return matching

    def _calc_rot_angle(self) -> float:
        def _check_angles_and_radiuses_are_identical(pps_A: PolarPointSet, pps_B: PolarPointSet) -> None | float:
            ang = abs(pps_A[0].phi - pps_B[0].phi)
            rot_ang = min(ang, 360 - ang)

            # print(rot_ang)

            # if abs(pps_A[0].r - pps_B[0].r) > 0.1:
            #     print()
            #     return False

            for i in range(1, self._N):
                cur_ang = abs(pps_A[i].phi - pps_B[i].phi)
                cur_rot_ang = min(cur_ang, 360 - cur_ang)

                # print(cur_rot_ang)

                if abs(cur_rot_ang - rot_ang) > 0.1 or (abs(pps_A[i].r - pps_B[i].r) > 0.1 and 0):
                    # print()
                    return False

            return rot_ang

        def _iterate() -> None:
            pass

        polar_point_set_A = self._A.convert_to_sorted_polar_point_set()
        polar_point_set_B = self._B.convert_to_sorted_polar_point_set()

        iteration_is_over = False
        count_iteration = 0

        while not iteration_is_over:
            count_iteration += 1

            # rot_ang = _check_angles_and_radiuses_are_identical(polar_point_set_A, polar_point_set_B)
            
            # if rot_ang:
            #     return rot_ang
            
            # _iterate()
            # polar_point_set_B.iterate()

            if count_iteration == self._N:
                iteration_is_over = True

        return None

    def show_points(self) -> None:
        fig, ax = plt.subplots()

        A_x = [p.x for p in self._A.points]
        A_y = [p.y for p in self._A.points]
        A_n = ['A_{}'.format(i + 1) for i in range(len(A_x))]
        
        B_x = [p.x for p in self._B.points]
        B_y = [p.y for p in self._B.points]
        B_n = ['B_{}'.format(i + 1) for i in range(len(B_x))]

        ax.scatter(A_x, A_y)

        for i, txt in enumerate(A_n):
            ax.annotate(txt, (A_x[i], A_y[i]))

        ax.scatter(B_x, B_y)

        for i, txt in enumerate(B_n):
            ax.annotate(txt, (B_x[i], B_y[i]))

        # plt.plot([p.x for p in self._A.points] + [p.x for p in self._B.points], [p.y for p in self._A.points] + [p.y for p in self._B.points], 'o')
        plt.show()


def point_pattern_matcher(A: list[tuple], B: list[tuple]) -> PointPatternMatcher:
    A_points = CartesianPointSet(CartesianPoints([CartesianPoint(p[0], p[1]) for p in A]))
    B_points = CartesianPointSet(CartesianPoints([CartesianPoint(p[0], p[1]) for p in B]))

    return PointPatternMatcher(A_points, B_points)