from math import sqrt, ceil

import matplotlib.pyplot as plt

from point_pattern_matcher import point_pattern_matcher


class Test:
    def __init__(self, A_pointss: list[list[tuple]], B_pointss: list[list[tuple]]):
        self.A_pointss = A_pointss
        self.B_pointss = B_pointss

        self.matchings = None

        self.size = len(self.A_pointss)

    def test(self) -> list[dict]:
        matchings = []

        for i in range(self.size):
            matchings.append(point_pattern_matcher(self.A_pointss[i], self.B_pointss[i]).match())
        
        self.matchings = matchings

        return matchings

    def plot(self):
        n = ceil(sqrt(self.size))

        fig, axs = plt.subplots(n, n, squeeze=False)

        for j in range(self.size):
            A_points = self.A_pointss[j]
            B_points = self.B_pointss[j]

            matchings = self.matchings if self.matchings is not None else self.test()

            A_x = [point[0] for point in A_points]
            A_y = [point[1] for point in A_points]
            A_n = ['A_{}'.format(i + 1) for i in range(len(A_x))]

            B_x = [point[0] for point in B_points]
            B_y = [point[1] for point in B_points]
            B_n = ['B_{}'.format(i + 1) for i in range(len(B_x))]

            axs[j // n, j % n].scatter(A_x, A_y, color="y")

            for i, txt in enumerate(A_n):
                axs[j // n, j % n].annotate(txt, (A_x[i], A_y[i]))

            axs[j // n, j % n].scatter(B_x, B_y, color="r")

            for i, txt in enumerate(B_n):
                axs[j // n, j % n].annotate(txt, (B_x[i], B_y[i]))

            for i in matchings[j].keys():
                axs[j // n, j % n].plot([A_points[i][0], B_points[matchings[j][i]][0]], [A_points[i][1], B_points[matchings[j][i]][1]], 'go', linestyle="--", markerfacecolor='none')

        plt.show()

def test_squarelike():
    A_points = [(2, 2), (2, 6), (2, 10), (10, 2), (10, 10)]
    # B_points = [(5, 6), (5, 10), (7, 10), (9, 6), (9, 10)]
    B_points = [(5, 6), (5, 8), (5, 10), (9, 6), (9, 10)]
    
    Test([A_points], [B_points]).plot()

def test_trapezoid():
    A_points = [(2, 2), (2, 4), (2, 6), (5, 1), (5, 7)]
    B_points = [(10, 11), (10, 12), (12, 11), (12, 13), (10, 13)]

    Test([A_points], [B_points]).plot()

def test_random():
    A_points = [(1, 1), (1, 5), (2, 8), (3, 0.2), (3, 6), (5, 5)]
    B_points = [(1.1, 0.9), (1, 5.5), (2.6, 8.4), (2.7, 0.3), (3.4, 5.9), (5.1, 5.2)]

    Test([A_points], [B_points]).plot()

def tests_2():
    A_points1 = [(2, 2), (2, 4), (2, 6), (5, 1), (5, 7)]
    B_points1 = [(10, 11), (10, 12), (12, 11), (12, 13), (10, 13)]

    A_points2 = [(1, 1), (1, 5), (2, 8), (3, 0.2), (3, 6), (5, 5)]
    B_points2 = [(1.1, 0.9), (1, 5.5), (2.6, 8.4), (2.7, 0.3), (3.4, 5.9), (5.1, 5.2)]

    A_pointss = [A_points1, A_points2]
    B_pointss = [B_points1, B_points2]

    Test(A_pointss, B_pointss).plot()

def tests_3():
    A_points1 = [(2, 2), (2, 4), (2, 6), (5, 1), (5, 7)]
    B_points1 = [(10, 11), (10, 12), (12, 11), (12, 13), (10, 13)]

    A_points2 = [(1, 1), (1, 5), (2, 8), (3, 0.2), (3, 6), (5, 5)]
    B_points2 = [(1.1, 0.9), (1, 5.5), (2.6, 8.4), (2.7, 0.3), (3.4, 5.9), (5.1, 5.2)]

    A_points3 = [(2, 2), (2, 6), (2, 10), (10, 2), (10, 10)]
    # B_points3 = [(5, 6), (5, 10), (7, 10), (9, 6), (9, 10)]
    B_points3 = [(5, 6), (5, 8), (5, 10), (9, 6), (9, 10)]

    A_pointss = [A_points1, A_points2, A_points3]
    B_pointss = [B_points1, B_points2, B_points3]

    Test(A_pointss, B_pointss).plot()

def tests_9():
    A_points1 = [(2, 2), (2, 4), (2, 6), (5, 1), (5, 7)]
    B_points1 = [(10, 11), (10, 12), (12, 11), (12, 13), (10, 13)]

    A_points2 = [(1, 1), (1, 5), (2, 8), (3, 0.2), (3, 6), (5, 5)]
    B_points2 = [(1.1, 0.9), (1, 5.5), (2.6, 8.4), (2.7, 0.3), (3.4, 5.9), (5.1, 5.2)]

    A_points3 = [(1, 3), (3, 2.5), (3, 4.5), (5, 1), (5, 3), (5, 5)]
    B_points3 = [(1.1, 3.2), (2.8, 2.7), (3.2, 4.4), (4.9, 1.2), (5.2, 2.9), (5.2, 4.9)]

    A_points4 = [(1, 1), (1, 5), (1, 7), (1, 8), (2.5, 4), (4, 6)]
    B_points4 = [(0.9, 1.2), (1.1, 5.2), (0.8, 7.3), (1.3, 7.7), (2.4, 4.1), (4.2, 6.1)]

    A_points5 = [(2, 2), (2, 6), (2, 10), (10, 2), (10, 10)]
    B_points5 = [(5, 6), (5, 8), (5, 10), (9, 6), (9, 10)]

    A_points6 = [(0, 0), (1, 1), (2, 2), (2.5, 0.5), (4, 7)]
    B_points6 = [(-0.1, 0.2), (0.9, 1.2), (1.9, 2.2), (2.4, 0.3), (3.7, 7.4)]

    A_points7 = [(-2, 1), (-2, 8), (0, 0), (1, 1), (2, 2), (2.5, 0.5), (4, 7)]
    B_points7 = [(-1.7, 1.2), (-2.3, 8.3), (-0.1, 0.2), (0.9, 1.2), (1.9, 2.2), (2.4, 0.3), (3.7, 7.4)]

    A_pointss = [A_points1, A_points2, A_points3, A_points4, A_points5, A_points6, A_points7]
    B_pointss = [B_points1, B_points2, B_points3, B_points4, B_points5, B_points6, B_points7]

    Test(A_pointss, B_pointss).plot()