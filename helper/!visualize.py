import matplotlib.pyplot as plt


def plot(A_points: list[tuple], B_points: list[tuple], matchings: dict):
    fig, ax = plt.subplots()

    A_x = [p[0] for p in A_points]
    A_y = [p[1] for p in A_points]
    A_n = ['A_{}'.format(i + 1) for i in range(len(A_x))]
        
    B_x = [p[0] for p in B_points]
    B_y = [p[1] for p in B_points]
    B_n = ['B_{}'.format(i + 1) for i in range(len(B_x))]

    ax.scatter(A_x, A_y, color="y")

    for i, txt in enumerate(A_n):
        ax.annotate(txt, (A_x[i], A_y[i]))

    ax.scatter(B_x, B_y, color="r")

    for i, txt in enumerate(B_n):
        ax.annotate(txt, (B_x[i], B_y[i]))

    for i in matchings.keys():
        plt.plot([A_points[i][0], B_points[matchings[i]][0]], [A_points[i][1], B_points[matchings[i]][1]], 'go', linestyle="--", markerfacecolor='none')

    plt.show()

def plots(plots_):
    fig, axs = plt.subplots(2)

    for j, p in enumerate(plots_):
        A_points = p[0]
        B_points = p[1]
        matchings = p[2]

        A_x = [p[0] for p in A_points]
        A_y = [p[1] for p in A_points]
        A_n = ['A_{}'.format(i + 1) for i in range(len(A_x))]
            
        B_x = [p[0] for p in B_points]
        B_y = [p[1] for p in B_points]
        B_n = ['B_{}'.format(i + 1) for i in range(len(B_x))]

        axs[j].scatter(A_x, A_y, color="y")

        for i, txt in enumerate(A_n):
            axs[j].annotate(txt, (A_x[i], A_y[i]))

        axs[j].scatter(B_x, B_y, color="r")

        for i, txt in enumerate(B_n):
            axs[j].annotate(txt, (B_x[i], B_y[i]))

        for i in matchings.keys():
            axs[j].plot([A_points[i][0], B_points[matchings[i]][0]], [A_points[i][1], B_points[matchings[i]][1]], 'go', linestyle="--", markerfacecolor='none')

    plt.show()