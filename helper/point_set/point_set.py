from warnings import filterwarnings

from cv2 import minEnclosingCircle
from numpy import array

from helper.point_set.points import CartesianPoints, PolarPoints, PolarPoint
from helper.point_set.point import CartesianPoint
from helper.point_set.circle import Circle

from helper.min_enc_circ import minimum_enclosing_circle


class PointSet:
    pass

class CartesianPointSet(PointSet):
    def __init__(self, points: CartesianPoints) -> None:
        self.points = points

    def __getitem__(self, key) -> CartesianPoint:
        return self.points[key]

    def __str__(self) -> str:
        return ", ".join([str(p) for p in self.points])

    def scale(self, s: float) -> None:
        self.points = CartesianPoints([CartesianPoint(s * p.x, s * p.y) for p in self.points])

    def transform(self, t: CartesianPoint) -> None:
        self.points = CartesianPoints([CartesianPoint(p.x + t.x, p.y + t.y) for p in self.points])    

    def calc_centroid(self) -> CartesianPoint:
        return self.points.avg()

    def calc_smallst_enclos_circle(self):
        arr = array([(p.x, p.y) for p in self.points])

        filterwarnings("error")
        try:
            center, radius = minEnclosingCircle(arr)
        except:
            center, radius = minimum_enclosing_circle(arr)
        
        return Circle(CartesianPoint(center[0], center[1]), radius)

    def convert_to_sorted_polar_point_set(self) -> 'PolarPointSet':
        return PolarPointSet(sorted(self._get_polar_points(), key=lambda pp: (pp.phi, pp.r)))

    def _get_polar_points(self) -> PolarPoints:
        return [p.convert_to_polar_point(self.calc_centroid()) for p in self.points]

    def subtract(self, point_set: CartesianPoints) -> 'CartesianPoints':
        return [p if not p in point_set else None for p in self]

class PolarPointSet(PointSet):
    def __init__(self, points: PolarPoints) -> None:
        self.points = points

    def __getitem__(self, key) -> PolarPoint:
        return self.points[key]

    def iterate(self) -> None:
        temp = self.points[-1]

        for i in range(len(self.points) - 1):
            self.points[i + 1] = self.points[i]

        self.points[0] = temp
