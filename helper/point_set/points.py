from helper.point_set.point import CartesianPoint, PolarPoint


class CartesianPoints(list[CartesianPoint]):
    def avg(self) -> CartesianPoint:
        return CartesianPoint(sum([cp.x for cp in self]) / len(self), sum([cp.y for cp in self]) / len(self))

class PolarPoints(list[PolarPoint]):
    pass
