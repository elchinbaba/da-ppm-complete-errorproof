from math import atan2, hypot, pi, sqrt

# from helper.helpers import CartesianPoints


class Point:
    pass

class CartesianPoint(Point):
    def __init__(self, x: float, y: float) -> None:
        self.x = x
        self.y = y

    def __str__(self) -> str:
        return str((self.x, self.y))

        return "({}, {})".format(self.x, self.y)

    def __sub__(self, other: 'CartesianPoint') -> 'CartesianPoint':
        return self.subtract(other)
    
    def distance(self, other: 'CartesianPoint' = None) -> float:
        if other is None:
            other = CartesianPoint(0, 0)

        return sqrt((self.x - other.x) * (self.x - other.x) + (self.y - other.y) * (self.y - other.y))

    def match(self, matching_points: list['CartesianPoint']) -> int:
        min_index = 0
        min_point = matching_points[min_index]
        min_distance = self.distance(min_point)

        for i in range(1, len(matching_points)):
            point_distance = self.distance(matching_points[i])

            if point_distance < min_distance:
                min_index = i
                min_point = matching_points[min_index]
                min_distance = point_distance

        return min_index

    def subtract(self, other: 'CartesianPoint') -> 'CartesianPoint':
        return CartesianPoint(self.x - other.x, self.y - other.y)

    def convert_to_polar_point(self, center: 'CartesianPoint' = None) -> 'PolarPoint':
        if center is None:
            center = CartesianPoint(0, 0)

        return PolarPoint(cartes_p=self, center=center)

class PolarPoint(Point):
    def __init__(self, r: float = None, phi: float = None, cartes_p: CartesianPoint = None, center: CartesianPoint = CartesianPoint(0, 0)) -> None:
        self.center = center

        if cartes_p is None:
            self.r = r
            self.phi = phi

            return

        self.r = self._calc_radius_by_cartesian(cartes_p, self.center)
        self.phi = self._calc_angular_by_cartesian(cartes_p, self.center)

    def __str__(self) -> str:
        return str((self.r, self.phi))

    def _calc_radius_by_cartesian(self, cartes_p: CartesianPoint, center: CartesianPoint) -> float:
        return hypot(cartes_p.x - center.x, cartes_p.y - center.y)

    def _calc_angular_by_cartesian(self, cartes_p: CartesianPoint, center: CartesianPoint) -> float:
        return 180 / pi * atan2(cartes_p.x - center.x, cartes_p.y - center.y)
