from helper.point_set.point_set import CartesianPointSet, PolarPointSet
from helper.point_set.points import CartesianPoints, CartesianPoint
from helper.min_enc_circ import minimum_enclosing_circle


def is_equal(a: float, b: float = 0, err = 10) -> bool:
    return abs(a - b) < err